package orm

import (
	"database/sql"
	"errors"
	"fmt"
	"reflect"
	_ "github.com/go-sql-driver/mysql"
)

type (
	DB struct {
		mysqldb *sql.DB
	}
)

func MySql(dbName, dbUser, dbPass string) (*DB, error) {
	mysqldb, err := sql.Open("mysql", fmt.Sprintf("%s:%s@/%s?parseTime=true&loc=Local", dbUser, dbPass, dbName))
	if err != nil {
		return nil, err
	}
	return &DB{mysqldb}, mysqldb.Ping()
}

// func MustMySql(dbName, dbUser, dbPass string) *DB {
// 	db, err := MySql(dbName, dbUser, dbPass)
// 	if err != nil {
// 		panic(err)
// 	}
// 	return db
// }

func (db *DB) Close() {
	db.mysqldb.Close()
}

func (db *DB) PrintAndExec(q string, args ...interface{}) error {
	printSql(q, args...)
	_, err := db.mysqldb.Exec(q, args...)
	return err
}

// Drop drops table. If tbl is string, then the table name is taken as is.
//
// If tbl is *struct, then the table name is taken from the struct type name in snake case. The struct's fields are not
// evaluated.
func (db *DB) Drop(tbl interface{}) error {
	tblName := mustBeTblName(tbl)
	q := fmt.Sprintf("DROP TABLE IF EXISTS %s", tblName)
	return db.PrintAndExec(q)
}

// It executes the sql command to create a table. The tbl must be *struct. Below is typical table definition:
//
// The uniques defines unique relationship between two or more fields in a table. See example.
func (db *DB) Table(tbl interface{}, uniques ...string) error {
	vStruct, tblName := mustBeTbl(tbl)

	// Generate query
	var columns, index string
	for i := 0; i < vStruct.NumField(); i++ {
		vi := vStruct.Field(i)
		ti := vStruct.Type().Field(i)

		colName := snakeCase(ti.Name)
		is := parseMapBool(ti.Tag.Get("is"))
		fk := ti.Tag.Get("fk")
		isFk := len(fk) != 0

		var colType string
		if colName == "id" {
			// Id field
			if vi.Kind() != reflect.Uint64 {
				return errors.New("id field must be uint64")
			}
			colType = "INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY"
		} else if isFk {
			// Foreign key field

			colType = "INT UNSIGNED"

			t := vi.Type().String()
			if len(t) >= 8 && t[:8] == "sql.Null" {
				// Nullable field
			} else {
				// Not null field
				if vi.Kind() != reflect.Uint64 {
					return fmt.Errorf("foreign key column \"%s\" must be uint64 or sql.NullInt64", colName)
				}
				colType += " NOT NULL DEFAULT 0"
			}

			index += fmt.Sprintf(
				"FOREIGN KEY (%s) REFERENCES %s(id) ON DELETE CASCADE ON UPDATE CASCADE,\n",
				colName, fk)
		} else {
			// Non id and non foreign key field

			if !is["unique"] {
				// Index this column because it is not unique
				index += fmt.Sprintf("INDEX(%s),\n", colName)
			}

			// sql.Null and time.Time
			var ok bool
			t := vi.Type().String()
			if len(t) >= 8 && t[:8] == "sql.Null" {
				// Nullable column
				colType, ok = colTypeNull[t]
			} else if t == "time.Time" {
				// Date and time
				colType = "DATETIME NOT NULL DEFAULT NOW()"
				ok = true
			} else {
				// Other not null column
				colType, ok = colTypeKind[vi.Kind()]
			}
			if !ok {
				return fmt.Errorf("column type \"%s/%s\" is unregistered", vi.Kind(), t)
			}
		}

		columns += fmt.Sprintf("%s %s", colName, colType)
		if is["unique"] {
			columns += " UNIQUE"
		}
		columns += ",\n"
	}
	if len(columns) == 0 {
		return nil // Empty table definition
	}
	for _, s := range uniques {
		index += fmt.Sprintf("UNIQUE(%s),\n", s)
	}
	colIdx := columns + index
	colIdx = colIdx[:len(colIdx)-2]
	q := fmt.Sprintf("CREATE TABLE %s(\n%s\n)", tblName, colIdx)

	// Execute query
	return db.PrintAndExec(q)
}

// Insert inserts data into table tbl and returns the id of last inserted data into the tbl.Id.
// The tbl must be of *struct and tbl.Id must be 0.
func (db *DB) Insert(tbl interface{}) error {
	vStruct, tblName := mustBeTbl(tbl)

	// Generate query

	fields := ""
	values := ""
	var args []interface{}

	walkTableNonDefaultValue(vStruct, false,
		// sqlNull
		func(fieldName string, value interface{}) {
			fields += fieldName + ","
			values += "?,"
			args = append(args, value)
		},
		// timeTime
		func(fieldName string, value interface{}) {
			fields += fieldName + ","
			values += "?,"
			args = append(args, value)
		},
		// def
		func(fieldName string, value interface{}) {
			if fieldName == "id" {
				panic(errors.New("id must be 0"))
			}
			fields += fieldName + ","
			values += "?,"
			args = append(args, value)
		})

	if len(fields) == 0 {
		return nil // no insert
	}
	q := fmt.Sprintf("INSERT INTO %s (%s) VALUES (%s)", tblName, fields[:len(fields)-1], values[:len(values)-1])

	// Execute query
	printSql(q, args...)
	rst, err := db.mysqldb.Exec(q, args...)
	if err != nil {
		return err
	}

	// Save id
	id, _ := rst.LastInsertId()
	vStruct.FieldByName("Id").SetUint(uint64(id))
	return nil
}

// Update updates data to database based on Id. The data to be updated are fields which are not equal to default value.
// To enforce update for default value field(s), use keys.
func (db *DB) Update(tbl interface{}, updateAllFields bool) error {
	vStruct, tblName := mustBeTbl(tbl)

	// Generate query

	var set string
	var args []interface{}
	var id interface{}

	walkTableNonDefaultValue(vStruct, updateAllFields,
		// sqlNull
		func(fieldName string, value interface{}) {
			set += fmt.Sprintf("%s=?,", fieldName)
			args = append(args, value)
		},
		// timeTime
		func(fieldName string, value interface{}) {
			set += fmt.Sprintf("%s=?,", fieldName)
			args = append(args, value)
		},
		// def
		func(fieldName string, value interface{}) {
			if fieldName == "id" {
				id = value
			} else {
				set += fmt.Sprintf("%s=?,", fieldName)
				args = append(args, value)
			}
		})

	if len(set) == 0 || id == nil {
		return nil // no update
	}
	set = set[:len(set)-1]
	q := fmt.Sprintf("UPDATE %s SET %s WHERE id=?", tblName, set)
	args = append(args, id)

	// Execute query
	return db.PrintAndExec(q, args...)
}

// ById gets one row of data based on id.
func (db *DB) ById(tbl interface{}, id uint64) error {
	vStruct, tblName := mustBeTbl(tbl)

	// Generate query
	sel, from, selArgs, _ := walkTableSelect(vStruct, tblName)
	q := fmt.Sprintf("SELECT %s %s WHERE id=?", sel, from)

	// Run the query
	printSql(q, id)
	return db.mysqldb.QueryRow(q, id).Scan(selArgs...)
}

func (db *DB) Where(cond *Condition) *Where { return &Where{db: db, cond: cond} }

// Delete deletes table tbl and returns number of rows affected. See "(*Where) Delete()" for information.
func (db *DB) Delete(tbl interface{}, rows *uint64) error {
	wh := &Where{db: db, cond: &Condition{cols: map[string]bool{}}}
	return wh.Delete(tbl, rows)
}

// See "(*Where) Count" notes.
func (db *DB) Count(arg interface{}, cnt *uint64) error {
	wh := &Where{db: db, cond: &Condition{cols: map[string]bool{}}}
	return wh.Count(arg, cnt)
}

// See "(*Where) ForEach" notes.
func (db *DB) ForEach(callback interface{}, distinct ...string) error {
	wh := &Where{db: db, cond: &Condition{cols: map[string]bool{}}}
	return wh.ForEach(callback, distinct...)
}

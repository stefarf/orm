package orm

import "reflect"

var (
	colTypeNull = map[string]string{
		"sql.NullString":  "VARCHAR(500)",
		"sql.NullInt64":   "INT",
		"sql.NullFloat64": "FLOAT",
		"sql.NullBool":    "BOOL",
	}

	colTypeKind = map[reflect.Kind]string{
		reflect.String: "VARCHAR(500) NOT NULL DEFAULT ''",
		reflect.Bool:   "BOOL NOT NULL DEFAULT 0",

		reflect.Int:   "INT NOT NULL DEFAULT 0",
		reflect.Int8:  "INT NOT NULL DEFAULT 0",
		reflect.Int16: "INT NOT NULL DEFAULT 0",
		reflect.Int32: "INT NOT NULL DEFAULT 0",
		reflect.Int64: "INT NOT NULL DEFAULT 0",

		reflect.Uint:   "INT UNSIGNED NOT NULL DEFAULT 0",
		reflect.Uint8:  "INT UNSIGNED NOT NULL DEFAULT 0",
		reflect.Uint16: "INT UNSIGNED NOT NULL DEFAULT 0",
		reflect.Uint32: "INT UNSIGNED NOT NULL DEFAULT 0",
		reflect.Uint64: "INT UNSIGNED NOT NULL DEFAULT 0",

		reflect.Float32: "FLOAT NOT NULL DEFAULT 0",
		reflect.Float64: "FLOAT NOT NULL DEFAULT 0",
	}
)

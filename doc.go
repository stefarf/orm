// Package orm is a simple Object Relational Mapping. It support sql.Nullxxx and time.Time.
//
// TODO 1
//
// Typical table definition:
//
// 		type TableName struct {
// 			Id   uint64
// 			Name string        `id:"unique"`
// 			Ref  uint64        `fk:"other_table"`
// 			Age  sql.NullInt64
// 			Updated time.Time
// 		}
//
// The table name and field name will be transformed to snake case. For example "FieldName" will be transformed into
// "field_name". Please be aware that "MYId" will be transformed into "m_y_id".
//
// The "Id" must be of type uint64 and it is the primary key. Tag `is:"unique"` tells that the field is unique. Tag
// `fk:"other_table"` tells that the field is a reference to foreign key of other_table. Foreign key field must be of
// type uint64 or sql.NullInt64.
//
// TODO 2
//
// There are 2 types of query definition, they are: table definition and join definition.
//
// Typical table definition:
//
// TODO
//
// Table definition struct must have "Id uint64" field. From above example, the Fruit will be transformed to snake case
// fruit and become the table name.
//
// Typical join definition:
//
// TODO
//
// Join definition struct must consist of more than one struct. Each has tag. For first struct, the "is" tag defines
// the "from <table>". For other struct, the "is" tag defines the join statement "<left/right/...> join <table>" and
// requires the "on" tag.
//
// From above example, the "L", "P", and "F" struct are short names of respective table.
package orm

package orm

import (
	"fmt"
	"github.com/pkg/errors"
)

type (
	Condition struct {
		exp  string
		cols map[string]bool
		args []interface{}
	}
)

func Cond(col, exp string, arg interface{}) *Condition {
	cond := &Condition{cols: map[string]bool{col: true}, exp: fmt.Sprintf("%s%s", col, exp)}
	if arg != nil {
		cond.args = []interface{}{arg}
	}
	return cond
}

func And(c ...*Condition) *Condition { return between("AND", c...) }
func Or(c ...*Condition) *Condition  { return between("OR", c...) }

func between(op string, c ...*Condition) *Condition {
	if len(c) < 2 {
		panic(errors.New("need at least 2 arguments"))
	}
	cond := &Condition{cols: map[string]bool{}}
	for i := 0; i < len(c); i++ {
		c := c[i]

		if i == 0 {
			cond.exp = fmt.Sprintf("(%s)", c.exp)
		} else {
			cond.exp = fmt.Sprintf("%s %s (%s)", cond.exp, op, c.exp)
		}
		for col := range c.cols {
			cond.cols[col] = true
		}
		cond.args = append(cond.args, c.args...)
	}
	return cond
}

package orm_test

import (
	"fmt"
	"bitbucket.org/stefarf/orm"
)

func ExampleWhere_Delete() {
	// Error checking is stripped off for clarity

	orm.IsPrintSql = false
	prepare()

	type (
		TblFruit struct {
			Id   uint64
			Name string `is:"unique"`
		}
	)

	db, err := orm.MySql(dbName, dbUser, dbPass)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer db.Close()

	db.Table(&TblFruit{})
	db.Insert(&TblFruit{Name: "Apple"})
	db.Insert(&TblFruit{Name: "Mango"})
	db.Insert(&TblFruit{Name: "Orange"})

	orm.IsPrintSql = true

	var cnt uint64
	db.Where(orm.Or(
		orm.Cond("name", "=?", "Mango"),
		orm.Cond("name", "=?", "Apple"),
	)).Delete(&TblFruit{}, &cnt)
	fmt.Println("Deleted:", cnt)
	// Output:
	// ### SQL ###
	// DELETE FROM fruit
	// WHERE (name=?) OR (name=?)
	// Args => [Mango Apple]
	// ###
	// Deleted: 2
}

func ExampleWhere_Count_table() {
	// Error checking is stripped off for clarity

	orm.IsPrintSql = false
	prepare()

	type (
		TblFruit struct {
			Id   uint64
			Name string `is:"unique"`
		}
	)

	db, err := orm.MySql(dbName, dbUser, dbPass)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer db.Close()

	db.Table(&TblFruit{})
	db.Insert(&TblFruit{Name: "Apple"})
	db.Insert(&TblFruit{Name: "Mango"})
	db.Insert(&TblFruit{Name: "Orange"})

	orm.IsPrintSql = true

	var cnt uint64
	db.Where(orm.Or(
		orm.Cond("name", "=?", "Apple"),
		orm.Cond("name", "=?", "Orange"),
	)).Count(&TblFruit{}, &cnt)
	fmt.Println(cnt)
	// Output:
	// ### SQL ###
	// SELECT COUNT(1) FROM fruit
	// WHERE (name=?) OR (name=?)
	// Args => [Apple Orange]
	// ###
	// 2
}

func ExampleWhere_ForEach_table() {
	// Error checking is stripped off for clarity

	orm.IsPrintSql = false
	prepare()

	type (
		TblFruit struct {
			Id   uint64
			Name string `is:"unique"`
		}
	)

	db, err := orm.MySql(dbName, dbUser, dbPass)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer db.Close()

	db.Table(&TblFruit{})
	db.Insert(&TblFruit{Name: "Apple"})
	db.Insert(&TblFruit{Name: "Mango"})
	db.Insert(&TblFruit{Name: "Orange"})

	orm.IsPrintSql = true

	db.Where(orm.Or(
		orm.Cond("name", "=?", "Apple"),
		orm.Cond("name", "=?", "Orange"),
	)).ForEach(func(data *TblFruit) {
		fmt.Println(*data)
	})
	// Output:
	// ### SQL ###
	// SELECT id,name FROM fruit
	// WHERE (name=?) OR (name=?)
	// Args => [Apple Orange]
	// ###
	// {1 Apple}
	// {3 Orange}
}

func ExampleWhere_First() {
	// Error checking is stripped off for clarity

	orm.IsPrintSql = false
	prepare()

	type (
		TblFruit struct {
			Id   uint64
			Name string `is:"unique"`
		}
	)

	db, err := orm.MySql(dbName, dbUser, dbPass)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer db.Close()

	db.Table(&TblFruit{})
	db.Insert(&TblFruit{Name: "Apple"})
	db.Insert(&TblFruit{Name: "Mango"})
	db.Insert(&TblFruit{Name: "Orange"})

	orm.IsPrintSql = true

	var data TblFruit
	db.Where(orm.Or(
		orm.Cond("name", "=?", "Apple"),
		orm.Cond("name", "=?", "Orange"),
	)).First(&data)
	fmt.Println(data)
	// Output:
	// ### SQL ###
	// SELECT id,name FROM fruit
	// WHERE (name=?) OR (name=?)
	// LIMIT 1
	// Args => [Apple Orange]
	// ###
	// {1 Apple}
}

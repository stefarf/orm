package orm

import "reflect"

var defaultValue = map[reflect.Kind]func(v reflect.Value) bool{
	reflect.String: defaultString,
	reflect.Bool:   defaultBool,

	reflect.Uint:   defaultUint,
	reflect.Uint8:  defaultUint,
	reflect.Uint16: defaultUint,
	reflect.Uint32: defaultUint,
	reflect.Uint64: defaultUint,

	reflect.Int:   defaultInt,
	reflect.Int8:  defaultInt,
	reflect.Int16: defaultInt,
	reflect.Int32: defaultInt,
	reflect.Int64: defaultInt,

	reflect.Float32: defaultFloat,
}

func defaultString(v reflect.Value) bool {
	return len(v.String()) == 0
}

func defaultBool(v reflect.Value) bool {
	return v.Bool() == false
}

func defaultUint(v reflect.Value) bool {
	return v.Uint() == 0
}

func defaultInt(v reflect.Value) bool {
	return v.Int() == 0
}

func defaultFloat(v reflect.Value) bool {
	return v.Float() == 0
}

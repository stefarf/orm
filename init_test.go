package orm_test

import (
	"fmt"
	"bitbucket.org/stefarf/orm"
)

func ExampleRecreateDB() {
	orm.IsPrintSql = true
	db, err := orm.MySQLAsRoot(rootPass)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer db.Close()
	orm.RecreateDB(db, dbName)
	// Output:
	// DROP DATABASE IF EXISTS mydb
	// CREATE DATABASE mydb
}

func ExampleRecreateUser() {
	orm.IsPrintSql = true
	db, err := orm.MySQLAsRoot(rootPass)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer db.Close()
	orm.RecreateUser(db, dbUser, dbPass)
	// Output:
	// DROP USER IF EXISTS myuser
	// CREATE USER myuser IDENTIFIED BY 'mypass'
}

func ExampleGrant() {
	orm.IsPrintSql = true
	db, err := orm.MySQLAsRoot(rootPass)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer db.Close()
	orm.Grant(db, dbUser, dbName)
	// Output:
	// GRANT ALL ON mydb.* TO myuser@'%'
}

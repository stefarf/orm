package orm_test

import (
	"fmt"
	"bitbucket.org/stefarf/orm"
)

const (
	rootPass = "root"
	dbName   = "mydb"
	dbUser   = "myuser"
	dbPass   = "mypass"
)

func prepare() {
	db, err := orm.MySQLAsRoot(rootPass)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer db.Close()
	orm.RecreateDB(db, dbName)
	orm.RecreateUser(db, dbUser, dbPass)
	orm.Grant(db, dbUser, dbName)
}

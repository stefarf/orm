package orm_test

import (
	"fmt"
	"bitbucket.org/stefarf/orm"
)

func Example_distinct() {
	// Error checking is stripped off for clarity

	orm.IsPrintSql = false
	// prepare()

	type (
		TblLikes struct {
			Person uint64
			Fruit  uint64
		}
		TblPerson struct {
			Id   uint64
			Name string
		}
		TblFruit struct {
			Id   uint64
			Name string
		}

		JoinSimple struct {
			L TblLikes
			P TblPerson `join:"left" on:"l.person=p.id"`
			F TblFruit  `join:"left" on:"l.fruit=f.id"`
		}
	)

	db, err := orm.MySql(dbName, dbUser, dbPass)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer db.Close()

	orm.IsPrintSql = true

	db.ForEach(func(data *JoinSimple) {
		fmt.Println(data.P.Name, "likes", data.F.Name)
	}, "p.name", "f.name")
	// Output:
	// SELECT DISTINCT p.name,f.name FROM likes l
	// LEFT JOIN person p ON l.person=p.id
	// LEFT JOIN fruit f ON l.fruit=f.id
}

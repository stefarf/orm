package orm

import (
	"fmt"
	"reflect"
	"strings"
)

type (
	Where struct {
		db   *DB
		cond *Condition
	}
)

// Delete deletes table and uses where to determine which are deleted.
//
// If tbl is string, then the table name is taken as is.
//
// If tbl is *struct, then the table name is taken from the struct type name in snake case. The struct's fields are not
// evaluated.
func (wh *Where) Delete(tbl interface{}, rows *uint64) error {
	// Walk the structure to generate query
	vStruct, tblName := mustBeTbl(tbl)
	_, _, _, selMap := walkTableSelect(vStruct, tblName)

	// Validate and generate where
	where := wh.validateAndGenerateWhere(selMap)

	// Generate query
	q := fmt.Sprintf("DELETE FROM %s", tblName)
	if len(where) != 0 {
		q += "\n" + where
	}

	// Execute query
	printSql(q, wh.cond.args...)
	rst, err := wh.db.mysqldb.Exec(q, wh.cond.args...)
	if err != nil {
		return err
	}
	if rows != nil {
		n, _ := rst.RowsAffected()
		*rows = uint64(n)
	}
	return nil
}

// Count executes select sql command based on query definition and returns the number of rows.
// The arg is query definition and must be *struct.
func (wh *Where) Count(arg interface{}, cnt *uint64) error {
	// Walk the structure to generate query
	var from string
	var selMap map[string]bool
	vStruct, isTbl, tblName := mustBeTblOrJoin(arg)
	if isTbl {
		_, from, _, selMap = walkTableSelect(vStruct, tblName)
	} else {
		_, from, _, selMap = walkJoinSelect(vStruct)
	}
	if len(from) == 0 {
		return nil // no query
	}

	// Validate and generate where
	where := wh.validateAndGenerateWhere(selMap)

	// Generate query
	q := fmt.Sprintf("SELECT COUNT(1) %s", from)
	if len(where) != 0 {
		q += "\n" + where
	}

	// Run the query
	printSql(q, wh.cond.args...)
	row := wh.db.mysqldb.QueryRow(q, wh.cond.args...)
	return row.Scan(cnt)
}

// ForEach executes select sql command and pass each row to callback function.
// The callback function must have "func(join *struct)" signature.
// The sql command is generated based on the structure of "join *struct".
//
// Please refer to "(*Where) Count" notes for query definition.
func (wh *Where) ForEach(callback interface{}, distinct ...string) error {
	// Walk the structure to generate query
	vCallback, vArg, vStruct, isTbl, tblName := mustBeCallback(callback)
	var sel, from string
	var selArgs []interface{}
	var selMap map[string]bool
	if isTbl {
		sel, from, selArgs, selMap = walkTableSelect(vStruct, tblName)
	} else {
		sel, from, selArgs, selMap = walkJoinSelect(vStruct)
	}
	if len(sel) == 0 {
		return nil // no query
	}

	// Validate and generate where
	where := wh.validateAndGenerateWhere(selMap)

	// Generate query
	var dis string
	if len(distinct) != 0 {
		dis = " DISTINCT"
		sel = strings.Join(distinct, ",")
	}
	q := fmt.Sprintf("SELECT%s %s %s", dis, sel, from)
	if len(where) != 0 {
		q += "\n" + where
	}

	// Run the query
	printSql(q, wh.cond.args...)
	rows, err := wh.db.mysqldb.Query(q, wh.cond.args...)
	if err != nil {
		return err
	}
	for rows.Next() {
		// Call the scan method
		err := rows.Scan(selArgs...)
		if err != nil {
			return err
		}

		// Call the callback function
		vCallback.Call([]reflect.Value{vArg})
	}

	return nil
}

// First executes select sql command based on query definition and returns the row or error if no data.
// The arg is query definition and must be *struct.
// The query is limited to 1 by using LIMIT 1.
//
// Please refer to "(*Where) Count" notes for query definition.
func (wh *Where) First(arg interface{}) error {
	// Walk the structure to generate query
	var sel, from string
	var selArgs []interface{}
	var selMap map[string]bool
	vStruct, isTbl, tblName := mustBeTblOrJoin(arg)
	if isTbl {
		sel, from, selArgs, selMap = walkTableSelect(vStruct, tblName)
	} else {
		sel, from, selArgs, selMap = walkJoinSelect(vStruct)
	}
	if len(sel) == 0 {
		return nil // no query
	}

	// Validate and generate where
	where := wh.validateAndGenerateWhere(selMap)

	// Generate query
	q := fmt.Sprintf("SELECT %s %s", sel, from)
	if len(where) != 0 {
		q += "\n" + where
	}
	q += "\nLIMIT 1"

	// Run the query
	printSql(q, wh.cond.args...)
	return wh.db.mysqldb.QueryRow(q, wh.cond.args...).Scan(selArgs...)
}

func (wh *Where) validateAndGenerateWhere(selMap map[string]bool) (where string) {
	if len(wh.cond.args) != 0 {
		for col := range wh.cond.cols {
			if !selMap[col] {
				panic(fmt.Errorf("col '%s' does not exist", col))
			}
		}
		where = "WHERE " + wh.cond.exp
	}
	return
}

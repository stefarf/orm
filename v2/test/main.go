package main

import (
	"bitbucket.org/stefarf/iferr"
	"bitbucket.org/stefarf/orm/v2"
)

func main() {
	dbName := "mysql"
	dbUser := "root"
	dbPass := "root"
	db, err := orm.MySql(orm.NewConn(dbName, dbUser, dbPass))
	iferr.Panic(err)
	defer db.Close()

	type (
		TblSome struct {
			orm.Base `unique:"name,age"`
			Owner    uint64 `fk:"user"`
			Name     string `is:"unique;not_null"`
			Age      uint   `default:"17" is:"not_null"`
		}
	)

	db.Create(&TblSome{}).PrintSql()
	db.DropIfExists(&TblSome{}).PrintSql()
	db.DropIfExists("table_name").PrintSql()

	db.Insert(&TblSome{Name: "Arief", Age: 17}).PrintSql()

	var cnt uint64
	var t TblSome

	db.DeleteById(&t, &cnt).Where(orm.Expr("name=?", "arief")).PrintSql() // Id != 0
	db.DeleteById(&t, nil).Where("name='arief'").PrintSql()               // Id != 0
	db.Delete(&TblSome{}).Where(orm.And(
		"name='arief'",
		orm.Expr("age>=? and age<=?", 17, 35),
		orm.Or("age=10", "age=11", "age=12"),
	)).PrintSql()
	db.Delete("table_name").PrintSql()

	db.First(&t).PrintSql()                             // select limit 1
	db.ForEach(func(t TblSome) {}).Limit(10).PrintSql() // select * limit n
	db.Count().PrintSql()                               // select count(*)

	tx := db.Tx()
	tx.XXX()
	tx.Rollback()
	tx.Commit()
}

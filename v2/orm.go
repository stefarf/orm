package orm

import (
	"database/sql"
	"github.com/go-sql-driver/mysql"
	"net"
	"bitbucket.org/stefarf/iferr"
)

type (
	Conn struct {
		DBName string
		User   string
		Passwd string
		Net    string
		Host   string
		Port   string
	}
	DB struct{ mydb *sql.DB }

	Base struct{ Id uint64 }
)

func NewConn(dbName, dbUser, dbPass string) *Conn {
	return &Conn{
		DBName: dbName,
		User:   dbUser,
		Passwd: dbPass,
		Net:    "tcp",
		Host:   "localhost",
		Port:   "3306",
	}
}

func MySql(conn *Conn) (*DB, error) {
	c := mysql.NewConfig()
	c.DBName = conn.DBName
	c.User = conn.User
	c.Passwd = conn.Passwd
	c.Net = conn.Net
	c.Addr = net.JoinHostPort(conn.Host, conn.Port)
	c.Params = map[string]string{
		"parseTime": "true",
		"loc":       "Local",
	}

	mydb, err := sql.Open("mysql", c.FormatDSN())
	iferr.Panic(err)
	iferr.Panic(mydb.Ping())
	return &DB{mydb: mydb}, nil
}

func (db *DB) Close() { db.mydb.Close() }

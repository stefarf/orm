package orm_test

import (
	"database/sql"
	"fmt"
	"bitbucket.org/stefarf/orm"
)

func Example_join() {
	// Error checking is stripped off for clarity

	orm.IsPrintSql = false
	prepare()

	type (
		TblPerson struct {
			Id   uint64
			Name string `is:"unique"`
			Age  sql.NullInt64
		}

		TblFruit struct {
			Id    uint64
			Name  string `is:"unique"`
			Notes sql.NullString
		}

		TblLikes struct {
			Id     uint64
			Person uint64 `fk:"person"`
			Fruit  uint64 `fk:"fruit"`
		}

		JoinSimple struct {
			L TblLikes
			P TblPerson `join:"left" on:"l.person=p.id"`
			F TblFruit  `join:"left" on:"l.fruit=f.id"`
		}
	)

	db, err := orm.MySql(dbName, dbUser, dbPass)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer db.Close()

	orm.IsPrintSql = true

	db.Table(&TblPerson{})
	db.Table(&TblFruit{})
	db.Table(&TblLikes{})

	arief := &TblPerson{Name: "Arief"}
	ana := &TblPerson{Name: "Ana"}
	karin := &TblPerson{Name: "Karin"}
	apple := &TblFruit{Name: "Apple"}
	mango := &TblFruit{Name: "Mango"}
	orange := &TblFruit{Name: "Orange"}

	db.Insert(arief)
	db.Insert(ana)
	db.Insert(karin)

	db.Insert(apple)
	db.Insert(mango)
	db.Insert(orange)

	db.Insert(&TblLikes{Person: arief.Id, Fruit: apple.Id})
	db.Insert(&TblLikes{Person: arief.Id, Fruit: mango.Id})
	db.Insert(&TblLikes{Person: ana.Id, Fruit: apple.Id})
	db.Insert(&TblLikes{Person: karin.Id, Fruit: orange.Id})

	wh := db.Where(orm.Or(
		orm.Cond("p.name", "=?", "Karin"),
		orm.Cond("f.name", "=?", "Apple"),
	))

	var cnt uint64
	wh.Count(&JoinSimple{}, &cnt)
	fmt.Println(cnt)

	wh.ForEach(func(data *JoinSimple) {
		fmt.Println(data.P.Name, "likes", data.F.Name)
	})
	// Output:
	// CREATE TABLE person(
	// id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	// name VARCHAR(500) NOT NULL DEFAULT '' UNIQUE,
	// age INT,
	// INDEX(age)
	// )
	// CREATE TABLE fruit(
	// id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	// name VARCHAR(500) NOT NULL DEFAULT '' UNIQUE,
	// notes VARCHAR(500),
	// INDEX(notes)
	// )
	// CREATE TABLE likes(
	// id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	// person INT UNSIGNED NOT NULL DEFAULT 0,
	// fruit INT UNSIGNED NOT NULL DEFAULT 0,
	// FOREIGN KEY (person) REFERENCES person(id) ON DELETE CASCADE ON UPDATE CASCADE,
	// FOREIGN KEY (fruit) REFERENCES fruit(id) ON DELETE CASCADE ON UPDATE CASCADE
	// )
	// ### SQL ###
	// INSERT INTO person (name) VALUES (?)
	// Args => [Arief]
	// ###
	// ### SQL ###
	// INSERT INTO person (name) VALUES (?)
	// Args => [Ana]
	// ###
	// ### SQL ###
	// INSERT INTO person (name) VALUES (?)
	// Args => [Karin]
	// ###
	// ### SQL ###
	// INSERT INTO fruit (name) VALUES (?)
	// Args => [Apple]
	// ###
	// ### SQL ###
	// INSERT INTO fruit (name) VALUES (?)
	// Args => [Mango]
	// ###
	// ### SQL ###
	// INSERT INTO fruit (name) VALUES (?)
	// Args => [Orange]
	// ###
	// ### SQL ###
	// INSERT INTO likes (person,fruit) VALUES (?,?)
	// Args => [1 1]
	// ###
	// ### SQL ###
	// INSERT INTO likes (person,fruit) VALUES (?,?)
	// Args => [1 2]
	// ###
	// ### SQL ###
	// INSERT INTO likes (person,fruit) VALUES (?,?)
	// Args => [2 1]
	// ###
	// ### SQL ###
	// INSERT INTO likes (person,fruit) VALUES (?,?)
	// Args => [3 3]
	// ###
	// ### SQL ###
	// SELECT COUNT(1) FROM likes l
	// LEFT JOIN person p ON l.person=p.id
	// LEFT JOIN fruit f ON l.fruit=f.id
	// WHERE (p.name=?) OR (f.name=?)
	// Args => [Karin Apple]
	// ###
	// 3
	// ### SQL ###
	// SELECT l.id,l.person,l.fruit,p.id,p.name,p.age,f.id,f.name,f.notes FROM likes l
	// LEFT JOIN person p ON l.person=p.id
	// LEFT JOIN fruit f ON l.fruit=f.id
	// WHERE (p.name=?) OR (f.name=?)
	// Args => [Karin Apple]
	// ###
	// Arief likes Apple
	// Ana likes Apple
	// Karin likes Orange
}

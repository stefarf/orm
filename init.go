package orm

import "fmt"

func MySQLAsRoot(rootPass string) (*DB, error) {
	return MySql("mysql", "root", rootPass)
}

// RecreateDB drops and then recreate database.
func RecreateDB(root *DB, dbName string) error {
	if err := root.PrintAndExec("DROP DATABASE IF EXISTS " + dbName); err != nil {
		return err
	}
	if err := root.PrintAndExec("CREATE DATABASE " + dbName); err != nil {
		return err
	}
	return nil
}

// RecreateUser drops and recreates user.
func RecreateUser(root *DB, dbUser, dbPass string) error {
	if err := root.PrintAndExec("DROP USER IF EXISTS " + dbUser); err != nil {
		return err
	}
	if err := root.PrintAndExec(fmt.Sprintf("CREATE USER %s IDENTIFIED BY '%s'", dbUser, dbPass)); err != nil {
		return err
	}
	return nil
}

// Grant grants the user access (all privileges) to database.
func Grant(root *DB, dbUser, dbName string) error {
	return root.PrintAndExec(fmt.Sprintf("GRANT ALL ON %s.* TO %s@'%%'", dbName, dbUser))
}

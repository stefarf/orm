package orm_test

import (
	"database/sql"
	"fmt"
	"os"
	"time"
	"bitbucket.org/stefarf/orm"
)

func ExampleDB_Drop() {
	// Error checking is stripped off for clarity

	type (
		TblPerson struct {
			Id   uint64
			Name string `is:"unique"`
		}
	)

	orm.IsPrintSql = true

	db, err := orm.MySql(dbName, dbUser, dbPass)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer db.Close()

	// Both are the same
	db.Drop("person")
	db.Drop(&TblPerson{})
	// Output:
	// DROP TABLE IF EXISTS person
	// DROP TABLE IF EXISTS person
}

func ExampleDB_Table() {
	// Error checking is stripped off for clarity

	type (
		TblPerson struct {
			Id      uint64
			Name    string `is:"unique"`
			Notes   sql.NullString
			Updated time.Time
		}

		TblFruit struct {
			Id   uint64
			Name string `is:"unique"`
		}

		TblLikes struct {
			Id     uint64
			Person uint64 `fk:"person"`
			Fruit  uint64 `fk:"fruit"`
		}
	)

	orm.IsPrintSql = false

	db, err := orm.MySql(dbName, dbUser, dbPass)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer db.Close()

	orm.IsPrintSql = true

	db.Table(&TblPerson{})
	db.Table(&TblFruit{})
	db.Table(&TblLikes{}, "person,fruit")
	// Output:
	// CREATE TABLE person(
	// id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	// name VARCHAR(500) NOT NULL DEFAULT '' UNIQUE,
	// notes VARCHAR(500),
	// updated DATETIME NOT NULL DEFAULT NOW(),
	// INDEX(notes),
	// INDEX(updated)
	// )
	// CREATE TABLE fruit(
	// id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	// name VARCHAR(500) NOT NULL DEFAULT '' UNIQUE
	// )
	// CREATE TABLE likes(
	// id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	// person INT UNSIGNED NOT NULL DEFAULT 0,
	// fruit INT UNSIGNED NOT NULL DEFAULT 0,
	// FOREIGN KEY (person) REFERENCES person(id) ON DELETE CASCADE ON UPDATE CASCADE,
	// FOREIGN KEY (fruit) REFERENCES fruit(id) ON DELETE CASCADE ON UPDATE CASCADE,
	// UNIQUE(person,fruit)
	// )
}

func ExampleDB_Insert() {
	// Error checking is stripped off for clarity

	orm.IsPrintSql = false
	prepare()

	type (
		TblPerson struct {
			Id   uint64
			Name string `is:"unique"`
			Age  sql.NullInt64
		}
	)

	db, err := orm.MySql(dbName, dbUser, dbPass)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer db.Close()

	orm.IsPrintSql = true

	db.Table(&TblPerson{})

	db.Insert(&TblPerson{Name: "Arief"})
	db.Insert(&TblPerson{Name: "Ana"})
	db.Insert(&TblPerson{Name: "Karin", Age: sql.NullInt64{2, true}})
	// Output:
	// CREATE TABLE person(
	// id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	// name VARCHAR(500) NOT NULL DEFAULT '' UNIQUE,
	// age INT,
	// INDEX(age)
	// )
	// ### SQL ###
	// INSERT INTO person (name) VALUES (?)
	// Args => [Arief]
	// ###
	// ### SQL ###
	// INSERT INTO person (name) VALUES (?)
	// Args => [Ana]
	// ###
	// ### SQL ###
	// INSERT INTO person (name,age) VALUES (?,?)
	// Args => [Karin {2 true}]
	// ###
}

func ExampleDB_Update() {
	// Error checking is stripped off for clarity

	orm.IsPrintSql = false
	prepare()

	type (
		TblPerson struct {
			Id   uint64
			Name string `is:"unique"`
			Age  uint
		}
	)

	db, err := orm.MySql(dbName, dbUser, dbPass)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer db.Close()

	db.Table(&TblPerson{})

	p := &TblPerson{Name: "Arief"}
	db.Insert(p)

	orm.IsPrintSql = true

	// Update will update non default value only. Age won't be updated because it is 0 (default value).
	db.Update(p, false)

	// Set enforces update all value
	db.Update(p, true)
	// Output:
	// ### SQL ###
	// UPDATE person SET name=? WHERE id=?
	// Args => [Arief 1]
	// ###
	// ### SQL ###
	// UPDATE person SET name=?,age=? WHERE id=?
	// Args => [Arief 0 1]
	// ###
}

func ExampleDB_Count_table() {
	// Error checking is stripped off for clarity

	orm.IsPrintSql = false
	prepare()

	type (
		TblFruit struct {
			Id   uint64
			Name string `is:"unique"`
		}
	)

	db, err := orm.MySql(dbName, dbUser, dbPass)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer db.Close()

	db.Table(&TblFruit{})
	db.Insert(&TblFruit{Name: "Apple"})
	db.Insert(&TblFruit{Name: "Mango"})
	db.Insert(&TblFruit{Name: "Orange"})

	orm.IsPrintSql = true

	var cnt uint64
	db.Count(&TblFruit{}, &cnt)
	fmt.Println(cnt)
	// Output:
	// SELECT COUNT(1) FROM fruit
	// 3
}

func ExampleDB_ForEach_table() {
	// Error checking is stripped off for clarity

	orm.IsPrintSql = false
	prepare()

	type (
		TblFruit struct {
			Id   uint64
			Name string `is:"unique"`
		}
	)

	db, err := orm.MySql(dbName, dbUser, dbPass)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer db.Close()

	db.Table(&TblFruit{})
	db.Insert(&TblFruit{Name: "Apple"})
	db.Insert(&TblFruit{Name: "Mango"})
	db.Insert(&TblFruit{Name: "Orange"})

	orm.IsPrintSql = true

	db.ForEach(func(data *TblFruit) {
		fmt.Println(*data)
	})
	// Output:
	// SELECT id,name FROM fruit
	// {1 Apple}
	// {2 Mango}
	// {3 Orange}
}

func ExampleDB_First() {
	// Error checking is stripped off for clarity

	orm.IsPrintSql = false
	prepare()

	type (
		TblPerson struct {
			Id   uint64
			Name string `is:"unique"`
			Age  uint
		}
	)

	db, err := orm.MySql(dbName, dbUser, dbPass)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer db.Close()

	db.Table(&TblPerson{})

	p := &TblPerson{Name: "Arief", Age: 17}
	db.Insert(p)

	orm.IsPrintSql = true

	var p2 TblPerson
	db.ById(&p2, p.Id)
	fmt.Println(p2)

	// Output:
	// ### SQL ###
	// SELECT id,name,age FROM person WHERE id=?
	// Args => [1]
	// ###
	// {1 Arief 17}
}

package orm

import (
	"errors"
	"fmt"
	"reflect"
	"regexp"
	"strings"
)

// Set to true to print sql command. Default value is false.
var IsPrintSql = false

// Regular expression to extract column names from "on" tag.
var reCols = regexp.MustCompile(`([a-z0-9._]+) *= *([a-z0-9._]+)`)

type colMap map[string]bool

func (cm colMap) from(on string) {
	for _, ss := range reCols.FindAllStringSubmatch(on, -1) {
		for _, col := range ss[1:] {
			cm[col] = true
		}
	}
}

func printSql(q string, args ...interface{}) {
	if IsPrintSql {
		if len(args) == 0 {
			fmt.Println(q)
		} else {
			fmt.Printf("### SQL ###\n%s\nArgs => %v\n###\n", q, args)
		}
	}
}

func parseMapBool(s string) map[string]bool {
	m := map[string]bool{}
	for _, s := range strings.Split(s, ",") {
		m[strings.ToLower(s)] = true
	}
	return m
}

func snakeCase(name string) string {
	lower := strings.ToLower(name)
	var rst []byte
	for i := range (name) {
		n := name[i]
		l := lower[i]
		if i != 0 && n != l {
			rst = append(rst, '_')
		}
		rst = append(rst, l)
	}
	return string(rst)
}

func mustBeTbl(tbl interface{}) (vStruct reflect.Value, tblName string) {
	err := errors.New("tbl must be *struct, contains 'Id uint64' and struct name begin with 'Tbl'")
	v := reflect.ValueOf(tbl)
	if v.Kind() != reflect.Ptr || v.Elem().Kind() != reflect.Struct {
		panic(err)
	}
	vStruct = v.Elem()
	_, isTbl := vStruct.Type().FieldByName("Id")
	if !isTbl {
		panic(err)
	}
	v = vStruct.FieldByName("Id")
	if v.Kind() != reflect.Uint64 {
		panic(err)
	}
	name := vStruct.Type().Name()
	if len(name) < 3 || name[:3] != "Tbl" {
		panic(err)
	}
	tblName = snakeCase(name[3:])
	return
}

func mustBeTblName(tbl interface{}) (tblName string) {
	err := errors.New("tbl must be string or *struct with name begin with 'Tbl'")
	v := reflect.ValueOf(tbl)
	if v.Kind() == reflect.String {
		tblName = v.String()
	} else if v.Kind() == reflect.Ptr && v.Elem().Kind() == reflect.Struct {
		name := v.Elem().Type().Name()
		if len(name) < 3 || name[:3] != "Tbl" {
			panic(err)
		}
		tblName = snakeCase(name[3:])
	} else {
		panic(err)
	}
	return
}

func mustBeTblOrJoin(arg interface{}) (vStruct reflect.Value, isTbl bool, tblName string) {
	v := reflect.ValueOf(arg)
	if v.Kind() != reflect.Ptr || v.Elem().Kind() != reflect.Struct {
		panic(errors.New("arg must be *struct"))
	}
	vStruct = v.Elem()
	name := vStruct.Type().Name()
	_, isTbl = vStruct.Type().FieldByName("Id")
	if isTbl {
		// Table
		v = vStruct.FieldByName("Id")
		if v.Kind() != reflect.Uint64 {
			panic(errors.New("field Id must be uint64"))
		}
		if len(name) < 3 || name[:3] != "Tbl" {
			panic(errors.New("struct name must begin with 'Tbl'"))
		}
		tblName = snakeCase(name[3:])
	} else {
		// Join
		if len(name) < 4 || name[:4] != "Join" {
			panic(errors.New("struct name must begin with 'Join'"))
		}
	}
	return
}

func mustBeCallback(callback interface{}) (vCallback, vArg, vStruct reflect.Value, isTbl bool, tblName string) {
	err := errors.New("callback must be func with signature func(*struct)")
	vCallback = reflect.ValueOf(callback)
	if vCallback.Kind() != reflect.Func || vCallback.Type().NumIn() != 1 {
		panic(err)
	}
	tArg := vCallback.Type().In(0)
	if tArg.Kind() != reflect.Ptr || tArg.Elem().Kind() != reflect.Struct {
		panic(err)
	}
	vArg = reflect.New(tArg.Elem())
	vStruct = vArg.Elem()
	name := vStruct.Type().Name()
	_, isTbl = vStruct.Type().FieldByName("Id")
	if isTbl {
		// Table
		v := vStruct.FieldByName("Id")
		if v.Kind() != reflect.Uint64 {
			panic(errors.New("field Id must be uint64"))
		}
		if len(name) < 3 || name[:3] != "Tbl" {
			panic(errors.New("struct name must begin with 'Tbl'"))
		}
		tblName = snakeCase(name[3:])
	} else {
		// Join
		if len(name) < 4 || name[:4] != "Join" {
			panic(errors.New("struct name must begin with 'Join'"))
		}
	}
	return
}

func walkTableSelect(vStruct reflect.Value, tblName string) (sel, from string, selArgs []interface{}, selMap map[string]bool) {
	selMap = map[string]bool{}
	from = fmt.Sprintf("FROM %s", tblName)

	// From table level struct, dig 1 level down
	for i := 0; i < vStruct.NumField(); i++ {
		vi := vStruct.Field(i)
		ti := vStruct.Type().Field(i)

		// Collect value for sql scan
		selArgs = append(selArgs, vi.Addr().Interface())

		// Construct select fields statement
		fieldName := snakeCase(ti.Name)
		sel += fieldName + ","
		selMap[fieldName] = true
	}
	sel = sel[:len(sel)-1]
	return
}

func walkJoinSelect(vStruct reflect.Value) (sel, from string, selArgs []interface{}, selMap map[string]bool) {
	err := errors.New("join definition must contains of struct(s) with type named 'Tbl...'")

	selMap = map[string]bool{}
	cols := colMap{}

	// From join level struct, dig 1 level down
	for i := 0; i < vStruct.NumField(); i++ {
		vi := vStruct.Field(i)
		ti := vStruct.Type().Field(i)

		if vi.Kind() != reflect.Struct {
			panic(err)
		}

		tblAlias := snakeCase(ti.Name)

		// Construct the from and join statement

		tblName := vi.Type().Name()
		if len(tblName) <= 3 || tblName[:3] != "Tbl" {
			panic(err)
		}
		tblName = snakeCase(tblName[3:])

		jo, ok := ti.Tag.Lookup("join")
		if !ok {
			jo = "LEFT" // default join
		} else {
			jo = strings.ToUpper(jo)
		}

		on := ti.Tag.Get("on")
		if len(on) == 0 {
			from += fmt.Sprintf("FROM %s %s\n", tblName, tblAlias)
		} else {
			from += fmt.Sprintf("%s JOIN %s %s ON %s\n", jo, tblName, tblAlias, on)
		}
		cols.from(on)

		// From table level struct, dig 1 level down
		for i := 0; i < vi.NumField(); i++ {
			vii := vi.Field(i)
			tii := vi.Type().Field(i)

			// Collect value for sql scan
			selArgs = append(selArgs, vii.Addr().Interface())

			// Construct select fields statement
			fieldName := fmt.Sprintf("%s.%s", tblAlias, snakeCase(tii.Name))
			sel += fieldName + ","
			selMap[fieldName] = true
		}
	}
	for col := range cols {
		if !selMap[col] {
			panic(fmt.Errorf("col '%s' does not exist", col))
		}
	}
	if len(sel) != 0 {
		sel = sel[:len(sel)-1]
	}
	from = from [:len(from)-1]
	return
}

func walkTableNonDefaultValue(
	vStruct reflect.Value, updateAllFields bool,
	sqlNull func(fieldName string, value interface{}),
	timeTime func(fieldName string, value interface{}),
	def func(fieldName string, value interface{}),
) {
	for i := 0; i < vStruct.NumField(); i++ {
		vi := vStruct.Field(i)
		ti := vStruct.Type().Field(i)

		fieldName := snakeCase(ti.Name)
		fieldType := vi.Type().String()

		if len(fieldType) >= 8 && fieldType[:8] == "sql.Null" {
			// Nullable field
			if vi.FieldByName("Valid").Bool() {
				// Not null value
				sqlNull(fieldName, vi.Interface())
			}
		} else if fieldType == "time.Time" {
			if !vi.MethodByName("IsZero").Call(nil)[0].Bool() {
				// Not null value
				timeTime(fieldName, vi.Interface())
			}
		} else {
			// Not null field
			k := vi.Kind()
			if isDef, ok := defaultValue[k]; ok {
				if updateAllFields || !isDef(vi) {
					def(fieldName, vi.Interface())
				}
			} else {
				panic(fmt.Errorf("%s is not registered", k))
			}
		}
	}
}
